#!/bin/bash

# Prepare Java on Debian
sudo apt-get install java-package java-common
make-jpkg ~/Downloads/server-jre-8u144-linux-x64.tar.gz
sudo dpkg -i oracle-java8-server-jre_8u144_amd64.deb

# Debian Atlassian SDK Installer
sudo sh -c 'echo "deb https://sdkrepo.atlassian.com/debian/ stable contrib" >>/etc/apt/sources.list'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys B07804338C015B73
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install atlassian-plugin-sdk
atlas-version

# You should be able to run 'atlas-run-standalone --product=jira' and have JIRA running locally on port 2990.
# Install Netdata
# Follow Netdata's [installation page in the Github Wiki](https://github.com/firehol/netdata/wiki/Installation). On Linux or macOS, you can quickly install Netdata:

bash <(curl -Ss https://my-netdata.io/kickstart.sh)
